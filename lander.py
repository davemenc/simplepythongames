#lander
START_H=1000.0
START_V=0.0
G = -9.8
STEP = 1.0
START_FUEL = 1000
t=float(0)
fuel = START_FUEL
v=START_V
h=START_H
while h>0 and fuel>0:
	t+=STEP
	v+=G/STEP
	h+=v
	print(f"Time: {t}; Height: {h}; Velocity: {v}; Fuel: {fuel}") 
	burn = input ("Enter burn (0-20)")
	if burn=="":
		burn = 0.0
	burn = float(burn)
	if burn<0:
		burn=0.0
	elif burn>20:
		burn=20.0

	v+=float(burn)
	fuel -=float(burn)
	print(f"Burn: {burn}")

print (f"Time: {t}; FINAL: Height: {h}; Velocity: {v}; Fuel: {fuel}")
