def get_float_from_human(prompt,min_val,max_val):
    done = False
    while not done:
        from_human = raw_input("{0} (must be between {1} and {2}): ".format(prompt,min_val,max_val))
        try:
            val = float(from_human)
            if val>max_val:
                print "{0} is too large; must not be more than {1}.".format(val,max_val)   
            elif val < min_val:
                print "{0} is too small; must not be less than {1}.".format(val,min_val)   
            else:
                done = True
        except ValueError:
            print "{} is not a valid number; please enter a valid number.".format(from_human)
    return val
def get_int_from_human(prompt,min_val,max_val):
    done = False
    while not done:
        from_human = raw_input("{0} (must be between {1} and {2}): ".format(prompt,min_val,max_val))
        try:
            val = int(from_human)
            if val>max_val:
                print "{0} is too large; must not be more than {1}.".format(val,max_val)   
            elif val < min_val:
                print "{0} is too small; must not be less than {1}.".format(val,min_val)   
            else:
                done = True
        except ValueError:
            print "{} is not a valid number; please enter a valid number.".format(from_human)
    return val
def get_str_from_human(prompt,min_len, max_len):
    done = False
    while not done:
        from_human = raw_input("{0} ({1} to {2} characters): ".format(prompt,min_len,max_len))
        if len(from_human)>max_len:
            print "{0} is too long; must be shorter than {1} characters.".format(from_human,max_len)   
        elif len(from_human) < min_len:
            print "{0} is too small; must not be less than {1}.".format(from_human,min_len)   
        else:
            done = True
    return val
   
for i in range(0,10):
    print get_float_from_human("Enter your age",4,99)
     
