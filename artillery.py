#!/usr/bin/python
import math,random

VALID = 0
NOT_A_NUMBER = 1
TOO_LARGE = 2
NEGATIVE = 3
TOO_SMALL = 4
def error_num_to_string(errornum,val):
	if errornum == NOT_A_NUMBER:
		return "{} is not a valid number; please enter a valid number.".format(val)
	if errornum == TOO_LARGE:
		return "{} is too large; try a smaller number.".format(val)
	if errornum == NEGATIVE:
		return "{} is negative; your burn must be a postive number.".format(val)
	if errornum == TOO_SMALL:
		return "{} is too small; try a larger number.".format(val)

def valid_input(input,max_val,min_val):
	#print "valid_input({0},{1},{2})".format(input,max_val,min_val),
	result = VALID
	try: 
		val = float(input)
		if val > max_val:
			result = TOO_LARGE
		if val < min_val:
			result = TOO_SMALL		
	except ValueError:
		result =  NOT_A_NUMBER
	return result

def get_human_input(prompt,min_val,max_val):
	user_in = raw_input("{0} (between {1} and {2}; enter={1}) ".format(prompt,min_val,max_val))
	if len(user_in)==0: user_in = min_val
	input_result = valid_input(user_in,max_val,min_val)
	while input_result != VALID:
		print error_num_to_string(input_result,user_in)
		print
		user_in = raw_input("{0} (between {1} and {2}; enter={1}) ".format(prompt,min_val,max_val))
		if len(user_in)==0: user_in = min_val
		input_result = valid_input(user_in,min_val,max_val)
	return float(user_in)
	
def main():
	G_CONSTANT = 9.81
	HIT_RADIUS = 20.0
	target = 500.0
	
	print "The target is {} m down range.".format(round(target,1))
	velocity = get_human_input("Enter Initial Velocity (m/s):",1.0,500.0)
	angle = get_human_input("Enter Firing Angle (degrees):",1.0,89.9)
	angle = angle*math.pi/180
	print "Range: {0}\nVelocity: {1}\nAngle: {2}\n".format(round(target,1),round(velocity),round(angle))
	d = velocity**2 * math.sin(2 * angle) / G_CONSTANT
	print d, velocity**2, math.sin(2*angle),G_CONSTANT
	proximity = abs(target-d)
	if d>target: description = "long"
	else: description = "short"
	print "The shell struck {0} down range which is {1} about {2}m. ".format(round(d,1),description ,round(proximity,0))
	if proximity<HIT_RADIUS/2:
		print "That was a very close hit! Target Destroyed!"
	elif proximity<=HIT_RADIUS: 
		print "That wasn't very close but the target was damaged."
	else:
		print "Miss. Better luck next time. "
		
	
if __name__ == "__main__": 
	main()