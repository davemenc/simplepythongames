VALID = 0
NOT_A_NUMBER = 1
TOO_LARGE = 2
NEGATIVE = 3
def error_num_to_string(errornum,val):
	if errornum == NOT_A_NUMBER:
		return "{} is not a valid number; please enter a valid number.".format(val)
	if errornum == TOO_LARGE:
		return "{} is too large; try a smaller number.".format(val)
	if errornum == NEGATIVE:
		return "{} is negative; your burn must be a postive number.".format(val)

def valid_input(input,engine_max):
	#print "valid_input",input,engine_max
	result = VALID
	try: 
		val = float(input)
		if val > engine_max:
			result = TOO_LARGE
		if val < 0:
			result = NEGATIVE
	except ValueError:
		result =  NOT_A_NUMBER
	return result

def get_human_burn(time, altitude, velocity, fuel, max_engine):
	#print "get_human_burn({0},{1},{2},{3},{4})".format(time, altitude,velocity,fuel,max_engine)
	if fuel<max_engine:
		max_engine = fuel
	print "At time {0}, Alt: {1}, Vel:{2}, Fuel: {3}".format(time,round(altitude,1),round(velocity,1),round(fuel,1))
	burn = raw_input("What is your burn (max:{0} Enter = 0)? ".format(max_engine))
	if len(burn)==0: burn = 0.0
	input_result = valid_input(burn,max_engine)
	while input_result != VALID:
		print error_num_to_string(input_result,burn)
		burn = raw_input("What is your burn (max:{0})? ".format(max_engine))
		input_result = valid_input(burn,max_engine)
	return float(burn)
def main():
	SIM_INTERVAL = 1
	START_ALTITUDE = 2000.0
	START_VELOCITY = 200.0
	G_CONSTANT = 9.8
	GRAVITY = G_CONSTANT* 0.167/SIM_INTERVAL # .167=moon, 1= earth
	USER_INTERVAL = 1
	ENGINE_MAX = 20.0 #this in acceration m/s/s; no idea what a reasonable number is here
	FUEL_MAX = 1000.0 #for simplicity this is in delta v so this is how much you can change your velocity, total
	MAX_TIME = 500
	MAX_LANDING_SPEED = 5

	time = 0
	altitude = START_ALTITUDE
	velocity = START_VELOCITY
	fuel = FUEL_MAX

	while time < MAX_TIME and altitude > 0 and fuel > 0:
		if time % USER_INTERVAL == 0:
			burn = get_human_burn(time, altitude,velocity,fuel, ENGINE_MAX)
		else:
			burn = 0
		velocity -= burn #burn is positive but reduces velocity
		velocity += GRAVITY
		altitude -= velocity #velocity is positive but reduces altitude
		fuel -= burn
		time += 1
	if altitude < 0:  altitude = 0
	print "Final results: "
	print "Time: {0}\nAlt: {1}\nVel:{2}\nFuel: {3}".format(time,round(altitude,1),round(velocity,1),round(fuel,1))
	if velocity > MAX_LANDING_SPEED: 
		print "Your speed of {0} exceeded the maximum safe landing speed of {1} so you are dead. RIP. ".format(round(velocity,1), MAX_LANDING_SPEED)
	else:
		print "Your speed of {0} was within the safe limits. Congratulation, your score is {1}. Low scores are best.".format(velocity,time)
	
if __name__ == "__main__": 
	main()