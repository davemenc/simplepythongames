"""Demo: how to call the IssueTracker api using issuetracker_client.py."""

from google3.google.devtools.issuetracker.v1 import issuetracker_builders
from google3.google.devtools.issuetracker.v1 import issuetracker_client
from google3.google.devtools.issuetracker.v1 import issuetracker_pb2
from google3.google.devtools.issuetracker.v1 import issuetracker_service_pb2
from google3.google.type import date_pb2
from google3.pyglib import app


# This key is provided for simplicity of trying out this example.
# You need to create your own API key, but once you have it you can
# hard code it into your application. go/issuetracker-api-key
API_KEY = 'AIzaSyBxfogNgbKUD0vcncynapFyk24Pdl9koH0'


def TypicalExample(client):
  """This demonstrates the typical usage pattern for issuetracker_service.

  response = client.stub.<Rpc>(
      issuetracker_service_pb2.<Rpc>Request(params))

  Args:
    client: An issuetracker_client.IssueTrackerClient object.
  """
  response = client.stub.GetHotlist(
      issuetracker_service_pb2.GetHotlistRequest(hotlist_id=210005))
  print( response)


def CreateIssueExample(client):
  """CreateIssue basic example.

  The IssueTracker pattern for creating issues is cumbersome in python, so
  the client provides a builder pattern for creating the request
  object.  All fields are supported; see the issuetracker_client.py code.

  Args:
    client: An issuetracker_client.IssueTrackerClient object.
  """
  # component is: Dev Tools > Buganizer (please only run in QA)
  builder = issuetracker_builders.CreateIssueRequestBuilder(component_id=1131)
  builder.SetTitle('issuetracker_client_demo')
  builder.SetComment('this is a comment')
  response = client.stub.CreateIssue(builder.Build())
  print (response)


def ModifyIssueExample(client):
  """ModifyIssue basic example.

  The IssueTracker pattern for modifying issues is cumbersome in python, so
  the client provides a builder pattern for creating the request
  object.  All fields are supported; see the issuetracker_builders.py code.


  Args:
    client: An issuetracker_client.IssueTrackerClient object.
  """
  builder = issuetracker_builders.ModifyIssueRequestBuilder(21196949)
  builder.SetAssignee('jkomarek@google.com')
  builder.SetStatus(issuetracker_pb2.Issue.ASSIGNED)
  response = client.stub.ModifyIssue(builder.Build())
  print (response)


def ModifyIssueCustomFieldExample(client):
  """ModifyIssue custom fields example.

  Hint: You can find the custom field ID on the
  issue (http://shortn/_HyhbwOLjTX) or in the component settings.

  Args:
    client: An issuetracker_client.IssueTrackerClient object.
  """
  builder = issuetracker_builders.ModifyIssueRequestBuilder(21196949)
  text_value = issuetracker_pb2.CustomFieldValue(custom_field_id=37165,
                                                 text_value='hello world')
  enum_value = issuetracker_pb2.CustomFieldValue(custom_field_id=37916,
                                                 enum_value='baz')
  date = date_pb2.Date(year=2016, month=1, day=1)
  date_value = issuetracker_pb2.CustomFieldValue(custom_field_id=37915,
                                                 date_value=date)
  builder.AddCustomFields([text_value, enum_value, date_value])
  response = client.stub.ModifyIssue(builder.Build())
  print (response)


# The following is for people transitioning off bugfu or
# another API that uses ints for priorities. For other users, you should use
# issuetracker_pb2.Issue.Priority.Value() or
# issuetracker_pb2.Issue.Priority.Name().
PRIORITY_TO_NUM = {
    # PRIORITY_UNSPECIFIED isn't here: it'll cause a KeyError
    issuetracker_pb2.Issue.P0: 0,
    issuetracker_pb2.Issue.P1: 1,
    issuetracker_pb2.Issue.P2: 2,
    issuetracker_pb2.Issue.P3: 3,
    issuetracker_pb2.Issue.P4: 4,
    }


def ListIssuesExample(client):
  """ListIssuesExample.

  List requests are paginated; the client provides iterators to
  handle paging through the response for list requests.

  Args:
    client: An issuetracker_client.IssueTrackerClient object.
  """
  query = 'assignee:jkomarek is:open created>2015-01-01 created<2015-06-01'
  for issue in issuetracker_client.ListIssuesIterator(
      client,
      issuetracker_service_pb2.ListIssuesRequest(query=query)):
    # See comment with PRIORITY_TO_NUM for priority as an int or a string
    print ('id:%d status:%s priority:%d(%s) %s' % (
        issue.issue_id,
        issuetracker_pb2.Issue.Status.Name(issue.issue_state.status),
        PRIORITY_TO_NUM[issue.issue_state.priority],
        issuetracker_pb2.Issue.Priority.Name(issue.issue_state.priority),
        issue.issue_state.title))


def main(unused_argv):
  # This example should never be run against prod.
  client = issuetracker_client.BuildDefault(use_prod=False, api_key=API_KEY)
  # TypicalExample(client)
  # ModifyIssueExample(client)
  # ModifyIssueCustomFieldExample(client)
  ListIssuesExample(client)
  # CreateIssueExample(client)


if __name__ == '__main__':
  app.run()
