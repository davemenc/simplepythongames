<?php
/* $Id$ */

 /*
  	Copyright 2009 Dave Menconi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
	$debug_flag = 0;
	$log_flag = 0;
	function debug_on(){
		global $debug_flag;
		$oldflag=$debug_flag;
		$debug_flag = 1;
		return $oldflag;
	}
	function debug_off(){
		global $debug_flag;
		$oldflag=$debug_flag;
		$debug_flag = 0;
		return $oldflag;
	}
	function debug_set($value){
		global $debug_flag;
		$oldflag=$debug_flag;
		$debug_flag = $value;
		return $oldflag;
	}
	function log_on(){
		global $log_flag;
		$oldflag=$log_flag;
		$log_flag=1;
		return $oldflag;
	}
	function log_off(){
		global $log_flag;
		$oldflag=$log_flag;
		$log_flag=0;
		return $oldflag;
	}
	function log_set($value){
		global $log_flag;
		$oldflag=$log_flag;
		$log_flag = $value;
		return $oldflag;
	}
	function clear_log(){
		global $log_flag;
		if(1==$log_flag){
			$fout=fopen("log.txt","wt");
			fwrite($fout,"___");
			fwrite($fout,date("Y-m-d H:i"));
			fwrite($fout,"___\n");
			fflush($fout);
			fclose($fout);
		}
	}
    function debug_string($name,$string=NULL){
        global $debug_flag, $log_flag;
        if(1==$debug_flag){
            if(!isset($string)){
                print "*".$name."\n";
            }else {
                print "*".$name.": ";
				print $string."\n";
            }
        }
		if (1==$log_flag){
			$fout=fopen("log.txt","at");
			fwrite($fout,"===");
			fwrite($fout,date("Y-m-d H:i"));
			fwrite($fout," ===\n");
			if(!isset($string)){
				fwrite($fout,"*");
				fwrite($fout,$name);
				fwrite($fout,":\n");
			}else{
				fwrite($fout,"*");
				fwrite($fout,$name);
				fwrite($fout,": ");
				fwrite($fout,$string);
				fwrite($fout,"\n");
			}
			fflush($fout);
			fclose($fout);
		}
    }
function debug_params($params){
	$fout=fopen("params.txt","at");
	foreach($params as $key=>$value){
		fwrite($fout,$key);
		fwrite($fout,"\n");
	}
	fflush($fout);
	fclose($fout);
}

    function debug_array($name,$array){
        global $debug_flag, $log_flag;
        if (1==$debug_flag){
            print "*".$name.":";
            print_r($array);
            print "";
        }
		if(1==$log_flag){
			$fout=fopen("log.txt","at");
			fwrite($fout,"---");
			fwrite($fout,date("Y-m-d H:i"));
			fwrite($fout," ---\n");
			fwrite($fout,"*");
			fwrite($fout,$name);
			fwrite($fout,":\nArray (");
			fflush($fout);
			if(isset($array)){
				foreach($array as $key=>$value){
					fwrite($fout,"[");
					fwrite($fout,$key);
					fwrite($fout,"]=>");
					fwrite($fout,$value);
				}
			}
			fwrite($fout,")\n");
			fflush($fout);
			fclose($fout);
		}
    }
	function debug_SERVER(){
		debug_array("Global Server Array",$_SERVER);
	}
function debug_show_var($var,$indent='  ',$niv='0'){
    global $debug_flag, $log_flag;

    $str="";
    if(is_array($var))    {
        $str.= "[array][".count($var)."]\n";
        foreach($var as $k=>$v)        {
            for($i=0;$i<$niv;$i++) $str.= $indent;
            $str.= "$indent\"{$k}\"=>";
            $str.=debug_show_var($v,$indent,$niv+1);
        }
    }
    else if(is_object($var)) {

        $str.= "[objet]-class=[".get_class($var)."]-method=[";
        $arr = get_class_methods($var);
           foreach ($arr as $method) {
               $str .= "[function $method()]";
           }
        $str.="]-";
        $str.="";
        $str.=show_php(get_object_vars($var),$indent,$niv+1);
    }
    else {
        $str.= "[".gettype($var)."]=[{$var}]\n";
    }
    if (1==$debug_flag){
    	print $str;
    }
	if(1==$log_flag){
		$fout=fopen("log.txt","at");
		fwrite($fout,"---");
		fwrite($fout,date("Y-m-d H:i"));
		fwrite($fout," ---\n");
		fwrite($fout,"*");
		fwrite($fout,":\nArray (");
		fflush($fout);
		fwrite($fout,$str);
		fwrite($fout,")\n");
		fflush($fout);
		fclose($fout);
	}

}
/*
 * $Log$
 */
?>
