import random
from datetime import date

today = date.today()

def show_rules():
	print()
	print("  I am thinking of a 3 digit number. \n  The first number can be 0.\n  The digits do not repeat\n  Try to guess my number and I will give you clues:")
	print("     PICO   - one digit correct but in the wrong position.")
	print("     FERMI  - one digit correct and in the right position.")
	print("     BAGELS - No digits correct.")
	print()
def pick_number():
	result = []
	digits = [0,1,2,3,4,5,6,7,8,9]
	for i in range(0,3):
		idx = random.randrange(len(digits))
		result.append(digits.pop(idx))

	return result
def str_to_list(str):
	result = []
	for c in str:
		result.append(int(c))
	return result

def list_to_str(l):
	result = ""
	for n in l:
		result += str(n)
	return result

def get_guess_result(guess,number):
	result = ""
	n = number.copy()
	g = guess.copy()
	guess = list(guess)
	if len(guess)>3: 
		return "Your guess was too long. Try again."
	if len(guess)<3:
		return "Your guess was too short. Try again."
	pico = 0
	fermi = 0
	for i in range(0,3):
		if n[i] in guess:
			if guess[i]==n[i]:
				fermi +=1 
				guess[i] = '-'
			else:
				pico += 1
	if pico+fermi==0:
		return "Bagels"
	else:
		for i in range(0,fermi):
			result += " Fermi"
		for i in range(0,pico):
			result += " Pico"
	return result

def add_to_leaders(leaders,guesses, date):
	guesses = str(guesses)
	if leaders is None:
		leaders={}
	if guesses in leaders:
		leaders[guesses].append((guesses,date))
	else:
		leaders[guesses]=[(guesses,date)]
	return leaders

def read_leader_board(fname):
	leaders = {}
	with open(fname,"rt") as f:
		for line in f:
			data = line.strip().split()
			guesses = int(data[0])
			date = data[1]
			leaders = add_to_leaders(leaders,guesses,date)
	return leaders

def write_leader_board(fname,leaders):
	TOTAL_LEADERS = 10
	if len(leaders)==0:
		return
	count = 0
	keys = list(leaders.keys())
	keynums=[]
	for k in keys:
		keynums.append(int(k))
	keynums = sorted(keynums)

	with open(fname,"wt") as f:
		for kn in keynums:
			k = str(kn)
			for l in leaders[k]:
				f.write(f"{k}\t{l[1]}\n")
				count += 1
				if count>=TOTAL_LEADERS:
					return

def show_leader_board(leaders):
	if len(leaders)==0:
		print("Leader Board Is Empty!")
		return
	print(f"\n  Bagels Leader Board ")
	print(" Guesses\tDate")
	keys = list(leaders.keys())
	keynums=[]
	for k in keys:
		keynums.append(int(k))
	keynums = sorted(keynums)

	for kn in keynums:
		k = str(kn)
		for l in leaders[k]:
			print(f"   {k}\t\t{l[1]}")
	print()
	

if __name__ == "__main__":
	
	LEADER_FILE =  "bagel_leaders.txt"
	MAX_GUESSES = 15

	leaders = read_leader_board(LEADER_FILE)
	#print(leaders)
	#leaders = add_to_leaders(leaders,5,today)
	#print(leaders)
	show_leader_board(leaders)
	#write_leader_board(LEADER_FILE,leaders)
	#leaders = read_leader_board(LEADER_FILE)
	#show_leader_board(leaders)
	exit()

	### Game Setup ###
	print("       Bagels Game")
	yn="n"
	yn = input(" Would you like the rules (y/n)? ")
	if len(yn)>0 and yn[0].lower()=="y":
		show_rules()
	print(f" You get {MAX_GUESSES} guesses.\n")
	number = pick_number()

	### Main Game Loop ###
	won = False
	for guess_count in range(MAX_GUESSES):
		guess = input(f" Guess #{guess_count+1}: ")
		if guess == "~~~":
			print("Secret handshake",number)
		else:
			guess = str_to_list(guess)
		guess_result = get_guess_result(guess,number)
		if str_to_list(guess)==number:
			print(f"\n  Congratulations! You won in {guess_count+1} guesses.")
			won=True
			break
		else:
			print(" ",guess_result)
	if not won:
		print("  After 15 guesses you didn't guess the 3 digits so you lost. Better luck next time.")
	guess_count+=1

	### Leader Board ###
	leaders = read_leader_board(LEADER_FILE)

	leaders = add_to_leaders(leaders,guess_count,today)
	write_leader_board(LEADER_FILE,leaders)

	#yn = input(" Would you like to see the leader board? ")
	#if len(yn)>0 and yn[0].lower()=="y":
	show_leader_board(leaders)