import webbrowser
import time
import sys
 
# Utility to open up Kanban and Critique in a new tab of current browser 
# v 0.01
# based on Chrome's Selenium browser tester controller
# first install Selenium for Chrome as: pip install -U Selenium
# Selenium is at (https://pypi.org/project/selenium/) 
#
# -----------------------------------------------------------------------------------------------
# Updates needed: 
# 1. Opens a new tab in current Chrome browser every X min 
#    Need to have it refresh rather than open new tab each time 
# 2. Load food options , one time, at 11:00am ?  https://eat.googleplex.com/nearby?meal=now 
#    if the current time is in between 11:00 and and 11:30am then pull up
# -----------------------------------------------------------------------------------------------
# run it as python load_urls.py , or with & in bg, do ps -ef , and kill -9 <pid> to stop the bg process

SEC_IN_MIN = 60
SEC_IN_HOUR = 3600
#----------- SET THESE 3 CONSTANTS ------------- #
SLEEP_TIME_MIN = 30 # time to sleep between URL refresh in Minutes
TIME_LIMIT_HOUR = 6 # time to run befor abort in hours 
#note: in the following line the LAST URL will be what the browser is showing
URLS = ["https://critique.corp.google.com/#/","https://buganizer.corp.google.com/savedsearches/5383489"]
#----------------------------------------------- #
SLEEP_TIME_SEC = SLEEP_TIME_MIN * SEC_IN_MIN
KILL_TIME_SEC = TIME_LIMIT_HOUR * SEC_IN_HOUR
end_time = time.time() + KILL_TIME_SEC
end_clock_time = time.ctime(end_time)
print("Running {} until {}".format(sys.argv[0],end_clock_time))
#webbrowser.get('chrome').open_new(a_website)

while time.time() < end_time:
    #to open in a tab
    for url in URLS:
        webbrowser.get('chrome').open_new_tab(url)
    time.sleep(SLEEP_TIME_SEC) #wait for SLEEP_TIME_MIN minutes

print('Ran for {} hours, exiting'.format(TIME_LIMIT_HOUR))
print("End time: ",time.ctime(end_time))
print("Cur Time: ",time.ctime(time.time()))
