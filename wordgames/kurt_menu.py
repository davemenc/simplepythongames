import csv

def read_file(in_file_name, out_file_name):
    METADATA_HEADERS = ['Simple', 'Shaken', 'Drink Name']

    with open(out_file_name,'wt') as out_file:
        out_file.write("ROBACCHUS MENU\n")
        with open(in_file_name,'rt') as in_file:
            csv_reader = csv.DictReader(in_file, delimiter='\t')
            for line in csv_reader:
                out_file.write(line['Drink Name'])
                out_file.write("\n" + "Ingredients" + "\n")
                #print(line)
                for ingredient in line:
                    if line[ingredient] != "":
                        if ingredient not in METADATA_HEADERS:
                            out_file.write("    {} oz {}\n".format(ingredient,
                                line[ingredient]))
                out_file.write("\n" + "Instructions" + "\n")
                if line['Shaken'] == 'n':
                    out_file.write("    Put ice in glass, add all ingredients and stir.\n")
                elif line['Shaken'] == 'y':
                    out_file.write("    Add ice and ingredients to a shaker and shake until cold. Pour into glass over ice.\n")
                else:
                    out_file.write("    No instructions provided.\n")
                out_file.write("-------------" + "\n\n")


read_file("menudata.tsv","menu2.txt")
