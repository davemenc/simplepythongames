import sys

def letter_freq(fname):
    freq = {'A':0,'B':0,'C':0,'D':0,'E':0,'F':0,'G':0,'H':0,'I':0,'J':0,'K':0,'L':0,'M':0,'N':0,'O':0,'P':0,'Q':0,'R':0,'S':0,'T':0,'U':0,'V':0,'W':0,'X':0,'Y':0,'Z':0}
    total = 0
    with open(fname,"rt") as f:
        for line in f:
            for c in line.upper():
                if c <= 'Z' and c >= 'A':
                    total += 1
                    freq[c] += 1
    for key in freq.keys():
        freq[key] = 100.0*float(freq[key])/float(total)
    return freq
 
if __name__ == "__main__":
    if len(sys.argv)<2:
        fname = "thousand.txt"
    else:
        fname = sys.argv[1]
    
    freq = letter_freq(fname)
    for letter,value in freq.items():
        print("{}\t{}".format(letter,round(value,2)))
