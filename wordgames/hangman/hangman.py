import json
import random
import string

def read_word_file(fname= "words.json"):
	#print(f"reading words from {fname}")
	with open(fname,"rt")as f:
		data = json.load(f)['data']
	return data
def get_valid_word(words,minlen=1):
	good_word = ""
	while good_word=="" or "-" in good_word or " " in good_word or len(good_word)<minlen: 
		good_word = random.choice(words).strip()
	return good_word.upper()
def display_progress(word,used_letters):
	#print(f"dp {word},{used_letters}")
	for c in word:
		if c in used_letters:
			print(f"{c}",end=" ")
		else:
			print(f"_",end=" ")
	print()
def hangman(words):
	word= get_valid_word(words)

	word_letters=set(word)
	alphabet = set(string.ascii_uppercase)
	used_letters = set()
	display_progress(word,used_letters)

	while len(word_letters)>0:
		user_letter = input("Guess a letter: ").upper()
		if user_letter in alphabet - used_letters:
			used_letters.add(user_letter)
			if user_letter in word_letters:
				word_letters.remove(user_letter)
			display_progress(word,used_letters)
		elif user_letter in used_letters:
			print('You have already used that character. Please guess a unique letter.')
		else:
			print('Invalid character. Please try again')
	return len(used_letters)-len(set(word))


if __name__== "__main__":
	fname= "words.json"
	words=read_word_file(fname)
	guesses = hangman(words)
	print(f"You Won!\nYou used {guesses} wrong guesses.")