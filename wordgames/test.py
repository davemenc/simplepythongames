# reads dates? 
import datetime
def parse_line(s):
    data = s.strip().split("\t")
    saying = data[0]
    status = data[1]
    date_s = data[2]
    if date_s.find("-") != -1: # this is iso-8601
        parts = date_s.split("-")
        date = datetime.datetime(int(parts[0]),int(parts[1]),int(parts[2]))
        #data_d["year"] = int(parts[0])
        #data_d["month"] = int(parts[1])
        #data_d["day"] = int(parts[2])
    elif date_s.find("/") != -1: # this is standard date (I'm American so it's m/d/y)
        parts = date_s.split("/")
        date = datetime.datetime(int(parts[2]),int(parts[0]),int(parts[1]))
        #data_d["month"] = parts[0]
        #data_d["day"] = parts[1]
        #data_d["year"] = parts[2]
    else:
        print("Bad date in line: ",s)
        date = None
    newdate = date.strftime("%Y-%m-%d")
    return {'saying':saying,'status':status,'date':newdate}

filename = "test.txt"
with open(filename,"rt") as f:
    for line in f:
        values  = parse_line(line)
        print(values)

