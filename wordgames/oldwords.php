<?php
/*
Copyright (c) 2009 Dave Menconi

*/
include_once "debug_win.php";
include_once "errors.php";

if ($argc<2) emiterror(1,$argc,__LINE__ );

$datafilename = $argv[1];

//print_r($argv);
//initialize variables


	$words = read_file($datafilename);//get the raw text from the file

$tab = "\t";
$nl= "\n";

 $consonants = "bcdfghjklmnpqrstvwxz";
 $vowels = "aeiou";

$starts=array();// starting strings
$ends = array();//ending strings
$voweldips = array();// vowel dipthongs



$wordcount =0;
//print "\n\n--------------------------\n\n";
foreach($words as $idx=>$word){
	$word = trim($word);
	if (strlen($word)==0) continue;

	$initial = initial_consonant($word);
	if ($initial=="") $initial = initial_vowel($word);
	if ($initial=="") print "*************** $word -- has no initial!\n";
	else if(array_key_exists($initial,$starts))$starts[$initial]['count']++;
	else $starts[$initial]['count']=1;
	$wordcount++;

	$startlen = strlen($initial);
	$rest = substr($word,$startlen);
	//print "$word = $initial+$rest\n";
	$core = initial_vowel($rest);
	if ($core=="")print "*********** $word -- $rest -- has no core!\n";
	else if(array_key_exists($core,$voweldips))$voweldips[$core]['count']++;
	else $voweldips[$core]['count']=1;


	$corelen = strlen($core);
	$rest = substr($rest,$corelen);
	//print "$word = $initial+$core+$rest\n";
	$end = initial_consonant($rest);
	if ($end=="");//print "*********** $word -- $rest -- has no End!\n";
	else if(array_key_exists($end,$ends))$ends[$end]['count']++;
	else $ends[$end]['count']=1;



}
$percentaccum = 0;
foreach($starts as $initial=>$data){
	$percent = 100 * $data['count']/$wordcount;
	$percentaccum += $percent;
	$starts[$initial]['percent']=$percent;
	$starts[$initial]['accum']=$percentaccum;
}
$percentaccum = 0;
foreach($voweldips as $initial=>$data){
	$percent = 100 * $data['count']/$wordcount;
	$percentaccum += $percent;
	$voweldips[$initial]['percent']=$percent;
	$voweldips[$initial]['accum']=$percentaccum;
}
$percentaccum = 0;
foreach($ends as $initial=>$data){
	$percent = 100 * $data['count']/$wordcount;
	$percentaccum += $percent;
	$ends[$initial]['percent']=$percent;
	$ends[$initial]['accum']=$percentaccum;
}
/*
print "wordcount = $wordcount\n";

print "-------------\n\nSTARTS\nInitial\tCount\tPercent\tAccum\n";
foreach ($starts as $char=>$data){
	print $char."\t".$data['count']."\t".$data['percent']."\t".$data['accum']."\n";
}

print "-------------\n\nVOWELS\nVowel\tCount\tPercent\tAccum\n";
foreach ($voweldips as $char=>$data){
	print $char."\t".$data['count']."\t".$data['percent']."\t".$data['accum']."\n";
}

print "-------------\n\nENDS\nEnd\tCount\tPercent\tAccum\n";
foreach ($ends as $char=>$data){
	print $char."\t".$data['count']."\t".$data['percent']."\t".$data['accum']."\n";
}
*/

for($i=0;$i<2000;$i++){
	$myrand = rand(1,10000)/100;
//	print "myrand1=$myrand\n";
	foreach($starts as $idx=>$data){
		$temp = $idx;
		if($myrand<=$data['accum']) break;
	}
	$keep = $temp;

		$myrand = rand(1,10000)/100;
//	print "myrand2=$myrand\n";
	foreach($voweldips as $idx=>$data){
		$temp = $idx;
		if($myrand<=$data['accum']) break;
	}
	$keep .=$temp;

	$myrand = rand(1,10000)/100;
//	print "myrand3=$myrand\n";
	foreach($ends as $idx=>$data){
		$temp = $idx;
		if($myrand<=$data['accum']) break;
	}
	$keep .=$temp;
	$keepers[]=$keep;
print "$keep\n";
}
//print "host\n";
//print_r($keepers);

/*
for ($i=0;$i<200;$i++){
	$word="";
	$wordlen=0;
	while($wordlen<8){
		$word .= $keepers[array_rand($keepers)];
//		print "word=$word\n";
		$wordlen=strlen($word);
	}
	print "$word\n";
}
*/
//--------- FUNCTIONS ----------
/** initial_vowel()
*
*/
function initial_vowel($s){
//	print "function initial_vowel($s)\n";
	$wordlen = strlen($s);
	$i=0;
	$initial = "";

	if (substr($s,0,1)=="y")return ("y");// y is special and confuses the program

	while($i<$wordlen && !is_consonant($letter = substr( $s,$i,1))){
		$initial .= $letter;
		$i++;
	}
	return($initial);
}


/** initial_consonant()
*
*/
function initial_consonant($s){
//	print "function initial_consonant($s)\n";
	$wordlen = strlen($s);
	$i=0;
	$initial = "";

	if (substr($s,0,1)=="y")return ("y");// y is special and confuses the program

	while($i<$wordlen && !is_vowel($letter = substr( $s,$i,1))){
		$initial .= $letter;
		$i++;
	}
	return($initial);
}
/** is_vowel()
*/
function is_vowel($s){
	global $vowels;
	//print "function is_vowel($s)\n";
	//print "vowels: $vowels\n";
	$s = substr($s,0,1); // it's supposed to be a single letter
	if(strpos($vowels,$s)===false) return false;
	else return true;
}

/** is_consonant()
*/
function is_consonant($s){
	global $consonants;
	//print "function is_vowel($s)\n";
	//print "consonants: $consonants\n";
	$s = substr($s,0,1); // it's supposed to be a single letter
	if(strpos($consonants,$s)===false) return false;
	else return true;
}
/** read_file()
 *
 *
 *
 *
 * @param $filename -- the file name to read data in from
 * @return $drink_distances -- the array data is stored in
 * @sideeffects populates $spec_drinks array
 *
 * @author Dave Menconi
 */

function read_file($filename){
//	print "function read_file($filename)\n";

	$s=getfile($filename);//get the raw text from the file
//	print "$filename: ".strlen($s)."\n";
	$linearray = explode  ( "\n" , $s);//split it into lines
//	print "linearray: ".count($linearray)."\n";
	return ($linearray);
}
//------------------------------------
/** putfile()
 * A routine the dumps text into a file
 *
 * @param $fname -- the fname to output to
 * @param $data -- the data to output
 * @return none
 * @sideeffects Another file on the disk
 * @author Dave Menconi
 */
function putfile($fname,$data){
	//print "function putfile($fname,data)\n";
	$fh=fopen($fname,'wt');
    if (false===$fh) emiterror(101,$fname,__LINE__ );
    $data = fwrite($fh,$data);
    if (false === $data) emiterror(103,$fname,__LINE__ );
    fclose($fh);
}
//------------------------------------
/** getfile()
 * Desc
 *
 * @param
 * @return
 * @author Dave Menconi
 */
function getfile($fname){
	//print "function getfile($fname)\n";
	if(!file_exists($fname)) emiterror(100,$fname,__LINE__ );
	$fh = fopen($fname,'rt');
    if (false===$fh) emiterror(101,$fname,__LINE__ );
    $data = fread($fh,filesize($fname));
    if (false === $data) emiterror(102,$fname,__LINE__ );
    fclose($fh);
    return $data;
}
//------------------------------------
/** printusage()
 * Desc
 *
 * @param
 * @return
 * @author Dave Menconi
 */
function printusage(){
	//print "function printusage()\n";
	print "Usage is \n";
	print "parseluciferlist <datafile>\n";
	print "Where <datafile> is the filename containing a tab-delimited text file\n";
	exit();
}

?>
