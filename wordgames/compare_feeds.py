import sys,csv
def compare_list(l1,l2):
    c1 = l1.copy()
    c2 = l2.copy()
    c1.sort()
    c2.sort()
    unique1 = []
    unique2 = []

    while len(c1)>0 and len(c2)>0:
        if c1[0]==c2[0]:
            del(c1[0])
            del(c2[0])
        elif c1[0]>c2[0]: # c2[0] is unique
            unique2.append(c2[0])
            del(c2[0])
        else: # c1[0] is unique
            unique1.append(c1[0])
            del(c1[0])
    return(unique1,unique2)

            
if len(sys.argv)<3:
    print("Bad invokation of {}.\nUsage:\n{} <fname1> <fname2>".format(sys.argv[0],sys.argv[0]))
    exit()
fname1 = sys.argv[1]
fname2 = sys.argv[2]
print(fname1,fname2)
idlist1 = []
idlist2 = []
with open(fname1, newline='', encoding='utf-8') as f:
    reader = csv.DictReader(f,delimiter="\t")
    print(reader)
    print(type(reader))
    print(dir(reader))
    fields1 = reader.fieldnames
    print(reader.dialect)
    print(reader.line_num)
    print(reader.reader)
    print(reader.restkey)
    print(reader.restval)
    for row in reader:
        idlist1.append(row['id'].strip())
with open(fname2, newline='', encoding='utf-8') as f:
    reader = csv.DictReader(f,delimiter="\t")
    print(reader)
    print(type(reader))
    print(dir(reader))
    fields2 = reader.fieldnames
    print(reader.dialect)
    print(reader.line_num)
    print(reader.reader)
    print(reader.restkey)
    print(reader.restval)
    for row in reader:
        idlist2.append(row['id'].strip())
(u1,u2) = compare_list(fields1,fields2)
print("unique attributes  to {}: \n{}\n\n".format(fname1,u1))
print("unique attributes  to {}: \n{}\n\n".format(fname2,u2))
(u1,u2) = compare_list(idlist1,idlist2)
print("unique IDs  to {}: \n{}\n\n".format(fname1,u1))
print("unique IDs  to {}: \n{}\n\n".format(fname2,u2))
