from PyDictionary import PyDictionary

# READ IN WORDS
wordtypes = ["Adverb","Noun","Adjective","Verb"] # each of these has a text file with the words in it
words={}
for type in wordtypes:
    words[type]=[] # create empty word list for that type 
    fname=type+".txt"
    with open (fname, "rt") as f:
        for line in f:
            line = line.strip().lower()
            if len(line)<=2:
                continue
            words[type].append(line)

# CHECK WORDS AGAINST PYDICTIONARY
dictionary=PyDictionary()
for type,words in words.items():
    print("Type: ",type)
    for word in words:
        meaning = dictionary.meaning(word)
        if meaning is None:
            print(f"*** {word} not in dictionary!")
            continue
        dict_type = list(meaning.keys())
        if len(dict_type)>1:
            print(f"Multiple parts: {word} {dict_type}")

        if type not in dict_type:
            print(f"{word}: {type}->{dict_type}")
exit()
