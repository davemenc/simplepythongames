from PyDictionary import PyDictionary
import re

class Sentence:
    skip =["whose","and","that","the","to","as","of","for","a","up","down","out","all","again","was","everywhere","is","are","it","her","which","against","at"]
    def __init__(self,line):
        self.line = line
        words = re.findall(r'\w+',line)
        self.words =[]
        self.positions = []
        self.parts_of_speech = []
        dictionary=PyDictionary()
        for w in words:
            word = w.lower()
            if word in Sentence.skip:
                continue
            meaning = dictionary.meaning(word)
            if meaning is None:
                self.parts_of_speech.append(["none"])
                print("Add to skip: ",word)
            else:
                self.parts_of_speech.append( list(meaning.keys()))
            self.positions.append(line.find(word))
            self.words.append(word)
    def __str__(self):
        result = self.line+"\n"
        for i in range(0,len(self.words)):
            result+=f"{self.words[i]}:{self.parts_of_speech[i]}, "
        result+="\n"
        return result

sayings = ["spider.txt","mary.txt","mary2.txt"]
text_fname = "mary.txt"
lines = []
with open(text_fname,"rt") as f:
    for line in f:
        s = Sentence(line.strip())
        lines.append(s)
for s in lines:
    print(s.line)
print()
for s in lines:
    l = s.line.lower()
    for i in range(0,len(s.words)):
        l=l.replace (s.words[i],str(s.parts_of_speech[i]))
    print( l)
