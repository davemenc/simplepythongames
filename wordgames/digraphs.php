<?php
/*
Copyright 2011 Dave Menconi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0
 */

$strategy = 2; // this is the number of characters to look at to pick the next character
$minwordlen = $strategy+3; // this is the minimum word size we can use
$setwordlen = 10; // if 0, pick based on average wordlen; otherwise constrain words to this word len

$filelistname = "filelist.txt";
//print "filelistname=$filelistname\n";
$pathname = 'D:\\DEV\\words\\textfiles\\';
$filenames = read_file($filelistname);//get the raw text from the file
$totalwordcount=0;
$filecount=0;
$wordcount = array();
foreach($filenames as $idx=>$fname){
	$filename = trim($fname);
	$filename = $pathname.$filename;
	$filename = $filename.".cnt";
	if(!file_exists($filename))continue;
	if(false == ($data = getfile($filename))) continue;//skip files we can't read
//print "--- $filename ---($filecount)\n";
	$filecount++;
	$wordlist = explode("\n",$data);
	foreach ($wordlist as $wordandcount){
		$word = trim(strstr($wordandcount,"\t",true));// first part before tab
		if(strlen($word)<$minwordlen) continue;
		$count = trim(strstr($wordandcount,"\t",false));//last part, after tab
		if(!array_key_exists($word,$wordcount))	$wordcount[$word] = $count;
		else $wordcount[$word] += $count;
		$totalwordcount += $count;
	}
//if($filecount>3) break;
}
//arsort($wordcount);
//print_r($wordcount);
//exit();
$initials = $wordlens = $finals = $frags = array();
foreach($wordcount as $word=>$count){
		$wordlen = strlen($word);

	if($wordlen<$minwordlen) continue; // skip words that are too short
		//print"--------------\n$word; ";
	//count word lenths
	if (array_key_exists($wordlen,$wordlens)) $wordlens[$wordlen]+=$count;
	else $wordlens[$wordlen]=$count;

	//count initials
	$init = substr($word,0,$strategy);
//	print "init=$init; ";
	if (array_key_exists($init,$initials)) $initials[$init]+=$count;
	else $initials[$init]=$count;

	// count finals
	$final = substr($word,$wordlen-1, 1);
	$semifinal = substr($word,$wordlen-$strategy-1,$strategy);
	//print "final===$final\n";
	if (array_key_exists($semifinal,$finals) && array_key_exists($final,$finals[$semifinal]))$finals[$semifinal][$final]+=$count;
	else $finals[$semifinal][$final]=$count;

	for($i=$strategy;$i<$wordlen-2; $i++){
//	print "($word) stratagey= $strategy; wordlen=$wordlen; i< $wordlen-$strategy; i=$i\n";
		$frag = substr($word,$i,$strategy);
		$targ = substr($word,$i+$strategy,1);
//	print "$frag:$targ; ";
		if (isset($frags[$frag][$targ]))$frags[$frag][$targ]+=$count;
		else $frags[$frag][$targ]=$count;
	}
//	print "\n";
}
print "stopping here\n";
print "\n****\ninitials:\n"; print_r($initials);
print "\n****\nwordlens:\n"; print_r($wordlens);
print "\n****\nfinals:\n"; print_r($finals);
print "\n****\nfrags:\n"; print_r($frags);
print "\n****\n";
exit();
/***************************************************/
$tot=0;
foreach ($wordlens as $key=>$value){
	$tot+=$value;
}
$wordlentot=$tot;


$tot=0;
foreach ($initials as $key=>$value){
	$tot+=$value;
}
$initialtot=$tot;


foreach ($finals as $key=>$value){
	$tot=0;
	foreach($value as $key2=>$value2){
			$tot+=$value2;
	}
	$finaltots[$key]=$tot;
}


foreach ($frags as $key=>$value){
	$tot=0;
	foreach($value as $key2=>$value2){
			$tot+=$value2;
	}
	$fragtot[$key]=$tot;
}





print "\n------ TOTAL UNIQUE WORDS ------\n";print count($wordcount)."\n";

//print "\n------ TOTALWORDCOUNT ------\n"; print_r($totalwordcount);
//print "\n------ WORDLENTOT ------\n"; print_r($wordlentot);
//print "\n------ WORDLENGTHS ------\n"; print_r($wordlens);
//print "\n------ INITIALTOT ------\n"; print"$initialtot\n";
//print "\n------ INITIALS ------\n";print"$initials\n";
//print "\n------ FINALTOTS ------\n"; print_r($finaltots);
//print "\n------ FINALS ------\n";print_r($finals);
//print "\n------ FRAGTOT ------\n"; print_r($fragtot);
//print "\n------ FRAGS ------\n";print_r($frags);
//print "\n------ wordcount ------\n";print_r($wordcount);

for($n=0; $n<1050; $n++){
	if ($setwordlen ==0)	$len =  randomarrayentry($wordlens,$wordlentot);
	else $len = $setwordlen ;

	//print "Length: $len ($setwordlen )\n";
	$initial = randomarrayentry($initials,$initialtot);
	//print "Initial: $initial\n";
	$myword = $initial;
	$current2 = $initial;
	for($i=1;$i<$len-2;$i++){
		if(array_key_exists($current2,$frags) && array_key_exists($current2,$fragtot) )$frag = randomarrayentry($frags[$current2],$fragtot[$current2]);
		else break;// give up and try again!
		$myword .= $frag;
		$current2 = substr($myword ,$i,2);
	//	print "$i: ($frag), $myword, $current2\n";
	}
	$current2 = substr($myword ,strlen($myword)-2,2);

//	print "($myword) last current2: $current2\n";
	if(array_key_exists($current2,$finals) && array_key_exists($current2,$finaltots) )$final = randomarrayentry($finals[$current2],$finaltots[$current2]);
	else $final = "";
	$myword .= $final;
	if (array_key_exists($myword,$dictionary)) continue;
	print "$myword\n";
//	print "Length: $len\n";
//	print "Initial: $initial\n";
//	print "final: $final\n";
}
exit();
//--------------------- FUNCTIONS ---------------------//
function randomarrayentry($list, $count){
	$r = mt_rand (1, $count);
	$total=0;
	foreach($list as $key=>$value){
		if($r>$total && $r<=$total+$value) return $key;
		$total += $value;
	}
	print "DONE: $key\n";
	return $key;
}
//----------------------------------------------------//
function getfile($fname){
	//debug_string("getfile($fname)");
	$fh = @fopen($fname,'r');
	if (false===$fh) return false;
	//debug_string("open worked");
	if(($fsize=filesize($fname))==0){fclose($fh); return false;}
	$data = fread($fh,$fsize);
	//debug_string("read worked");
	fclose($fh);
 return $data;
}
/** read_file()
 *
 *
 *
 *
 * @param $filename -- the file name to read data in from
 * @return $drink_distances -- the array data is stored in
 * @sideeffects populates $spec_drinks array
 *
 * @author Dave Menconi
 */

function read_file($filename){
//	print "function read_file($filename)\n";

	$s=getfile($filename);//get the raw text from the file
	$linearray = explode  ( "\n" , $s);//split it into lines
	return ($linearray);
}
//------------------------------------
function read_dictionary($fname){
	$list = read_file("dictionary.txt");
	$dictionary= array_flip ( $list);

	return $dictionary;
}

?>