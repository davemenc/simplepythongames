import random
import string
class Genome:
    ideal = None
    def __init__(self,organism):
        self.rating = self.rate_organism()

    def __init__(self,organism=None):
        if organism is None:
            self.organism = self.random_organism()
        else:
            self.organism = organism
        self.rating = self.rate_organism()
    def __repr__(self):
        return repr((self.organism,round(self.rating,2)))

    def rate_organism(self):
        rate = 0
        for idx,char in enumerate(self.organism):
            if char == Genome.ideal[idx]:
                rate += 1
        return float(rate)/float(len(Genome.ideal))

    def random_organism(self):
        return  ''.join([random.choice(string.ascii_letters+" " ) for n in range(len(Genome.ideal))]) 

if __name__ == "__main__":
    goal = "Hello World"+string.ascii_letters
    Genome.ideal = goal
    population = [Genome(),Genome(),Genome(),Genome(),Genome(),Genome()]
    newpop = sorted(population,key=lambda gene: gene.rating,reverse=True)
    print (newpop[:5])
