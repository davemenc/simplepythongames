# class that simulates the universe
from genome import Genome 
import random
import string

class Universe:
    
    def __init__(self,pop_size,max_generations,mutation=.01,new_generation=.3):
        self.pop_size = pop_size
        self.max_generations = max_generations
        self.mutation_rate = mutation
        self.new_generation = new_generation
        self.population = []
        self.total_rating = 0.0
        self.generation = 0

        for i in range(pop_size):
            self.population.append(Genome())
        self.sort_population()
    def __repr__(self):
        return repr((len(self.population),round(self.population[0].rating,2),round(self.total_rating),self.generation,self.population[:5]))
       
    def sum_ratings(self):
        total = 0.0
        for genome in self.population:
            total += genome.rating
        self.total_rating = total

    def mutate(self,genes):
        genelen = len(genes)
        mutation_position = random.randint(0,len(genes)-1)
        mutation = random.choice(string.ascii_letters+" " )
        newgenes = genes[:mutation_position] + mutation + genes[mutation_position+1:]
        if len(newgenes) != genelen:
            print("MUTATION CAUSED BAD GENE!",genlen,mutation_position, mutation, newgenes)
            print(genes[:mutation_position],mutation,genes[mutation_position+1:])
            exit()
        return newgenes

    def mate(self,mom,dad):
        mom_genes = mom.organism
        dad_genes = dad.organism
        if len(dad_genes)!=len(mom_genes):
            print("MOM AND DAD GENES CAN'T MATE! ",mom,dad)
            exit()
        org_len = len(mom_genes)
        crossover = random.randint(1,org_len-1)
        baby_genes = mom_genes[0:crossover]+dad_genes[crossover:]
        if len(baby_genes) != len(mom_genes):
            print("BABY GENES WRONG LEN!",mom_genes,dad_genes,baby_genes)
            print(org_len,crossover,mom_genes[0:crossover],dad_genes[crossover:])
        if random.random()<self.mutation_rate:
           baby_genes = self.mutate(baby_genes) 
        return Genome(baby_genes)
        
        
    def find_mate(self,mate=None):
        goal = random.uniform(0, self.total_rating)
        walk = 0.0
        for idx,genome in enumerate(self.population):
            walk += genome.rating
            if walk>=goal:
                break
        if mate==genome :
            if idx+1<len(self.population):
                result = self.population[idx+1]
            else:
                result = self.population[idx-1]
        else:
            result = genome
        if mate==result:
            print( "------------> INCEST@@@",mate,result,genome,self.population[idx+1])
        return result
        
    def do_mating(self):
        self.sum_ratings()
        new_pop = []
        total_births = int(self.new_generation*self.pop_size)
        for i in range(0,total_births):
            mom = self.find_mate()
            dad = self.find_mate(mom)
            baby = self.mate(mom,dad)
            if len(baby.organism) != len(dad.organism):
                print ("BROKEN MATING!",mom,dad,baby)
                exit()
            #print("mom,dad,baby",mom,dad,baby)
            new_pop.append(baby)
        
        for i in range(len(self.population)-total_births,len(self.population)):
            del(self.population[-1])
        self.population.extend(new_pop)
        self.generation += 1
        self.sort_population()

    def sort_population(self):
        self.population = sorted(self.population,key=lambda gene: gene.rating,reverse=True)
    
if __name__ == "__main__":
    goal = "Hello World" 
    Genome.ideal = goal 
    universe = Universe(1000,500,0.0)
    while universe.generation < universe.max_generations and universe.population[0].organism!=goal:
        print("GENERATION: {}".format(universe.generation) )
        print(universe)
        #print(universe.population)
        print()
        universe.do_mating()
#        input("Continue")

    print()
    print('The best we found in {} generations is "{}" with a rating of {}.'.format(universe.generation,universe.population[0].organism,round(universe.population[0].rating,2)))
    print(universe)
    

