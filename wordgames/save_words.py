# attempt to find the closes word
import sys
def digrams(w):
    w = w.upper()
    result = {"$"+w[0]}
    for i in range(0,len(w)):
        dg = w[i:i+2]
        if len(dg)==1:
            dg += "$"
        result.add(dg)
    return result
def convert_digram_to_string(digram):
    dg = list(digram)
    dg.sort()
    s = ""
    for d in dg:
        s += d+","
    return s[:-1]
    
def read_write_words(infname,outfname):
    unique = {}
    inf = open(infname,"rt")
    outf = open(outfname,"wt")

    for word in inf:
        word = word.upper().strip()
        if word in unique:
            continue
        unique[word] = None
        l = len(word)
        dg = digrams(word)
        s = convert_digram_to_string(dg)
        outf.write("{}\t{}\t{}\n".format(word,l,s))
    inf.close()
    outf.close()
def read_words(infname):
    wordlist = {}
    with open(infname, "rt") as f:
        for line in f:
            data = line.strip().split("\t")
            word = data[0]
            l = int(data[1])
            dg_str = data[2]
            dg = set(dg_str.split(","))
            if l not in wordlist:
                wordlist[l] ={} 
            wordlist[l][dg_str] =[word,dg]
    return wordlist

def jaccard(w1,w2):
    s1 = digrams(w1)
    s2 = digrams(w2)
    u = len(set.union(s1,s2))
    if u == 0:
        return sys.maxsize
    return len(set.intersection(s1,s2)) / u
def convert_str_to_digram(s):
    l = s.split(",")

def find_closest(word,wordlist):
    word = word.upper().strip()
    l = len(word)
    dg = digrams(word)
    s = convert_digram_to_string(dg):
    if s in wordlist[l]:
        return wordlist[l][s]
    

if __name__ == "__main__":
    infname = "/usr/share/dict/words"
    outfname = "wordlist.txt"
    #read_write_words(infname,outfname)
    wordlist = read_words(outfname)
    words = ["firefox","aardvark","ardvark"]
    for word in words: 
        print(word,find_closest(word,wordlist))

