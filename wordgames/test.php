<?php
$wordcountlist = array();
$filelist = array("temp.txt", "foo.bar", 'dummy.txt','bad.txt');
foreach ($filelist as $fname){
	print "opening $fname\n";
	$fin = fopen($fname,"rt");
	if ($fin===false){
		print "*** error: could not open $fname for reading. \n";
		continue;
	}
	else print "successfully opened $fname\n";

	while (($line = fgets($fin,8000))!== false ){
		parse_line($line);
	}
	fclose($fin);
}
print_r($wordcountlist);
function parse_line($line){
	global $wordcountlist;
	//print "function parse_line($line)\n";
	$line = strtolower($line);
	$line = preg_replace('/[^ a-z]+/',' ', $line);
	$words = explode(" ",$line);
	foreach($words as $word){
		if(strlen($word)==0) continue;
		if(array_key_exists ($word,$wordcountlist))$wordcountlist[$word]++;
		else $wordcountlist[$word]=1;
	}
}
?>