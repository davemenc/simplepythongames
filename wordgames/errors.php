<?php
//------------------------------------
/** emiterror()
 * Desc
 *
 * @param
 * @return
 * @author Dave Menconi
 */

function emiterror($errorno,$s="", $lineno, $exit=true){
//emiterror(0,"", __LINE__, $exit=true);
	//print "function emit($errorno,$s, $lineno)\n";
	// takes an error # and a string specific to the error and prints out an error message specific to that error number
//	print "\nError# $errorno in line $lineno with information \"$s\".\n";
	switch ($errorno){
		case "1":
		    $errormsg="Not enough arguments. Only $s found. \n";
		    print $errormsg;
		    printusage();
			break;
		case "100":
			$errormsg="Cannot find filename $s.\n";
			break;
		case "101":
			$errormsg="Cannot open filename $s.\n";
			break;
		case "102":
			$errormsg="Cannot read filename $s.\n";
			break;
		case "103":
			$errormsg="Cannot write filename $s.\n";
			break;
		case "200":
			$errormsg="Not enough drink names. Only found $s.\n";
			break;
		case "201":
			$errormsg="Attribute flag \"$s\" not found.\n";
			break;
		case "202":
			$errormsg="Bad data: \"$s\".\n";
			break;
		case "300":
			$errormsg="Missing average. $s.\n";
			break;
		case "301":
			$errormsg="Requested average but drink names were the same. $s.\n";
			break;
		case "302":
			$errormsg="Value should be an array but is: $s.\n";
			break;
		case "400":
			$errormsg="INCEST! Trying to mate the same drink with itself. $s.\n";
			break;
		case "500":
			$errormsg="Unknown filename $s.\n";
			break;
		case "501":
			$errormsg="The two versions of the  \"\$$s\" array don't match.\n";
			break;
		case "502":
			$errormsg="Null drink, attribute or ingredient name:  $s.\n";
			break;
		case "503":
			$errormsg="Drink, attribute or ingredient name with an invalid character in it: $s.\n";
			break;
		case "504":
			$errormsg="Drink, attribute or ingredient name consisting only of spaces: $s.\n";
			break;
		case "505":
			$errormsg="Drink, attribute or ingredient name is too long: $s.\n";
			break;
		case "600":
			$errormsg="Species program could not resolve status of drink combination $s.\n";
			break;
		case "601":
			$errormsg="Screwed up crossover indexing in species program: $s\n";
			break;
		case "602":
			$errormsg="Crossover list empty!.\n";
			break;
		case "603":
			$errormsg="Combinedcrossover list empty!.\n";
			break;
		case "000":
			$errormsg="Invalid error number. $s.\n";
			break;
		case "000":
			$errormsg="Invalid error number. $s.\n";
			break;
		case "000":
			$errormsg="Invalid error number. $s.\n";
			break;
		case "000":
			$errormsg="Invalid error number. $s.\n";
			break;
		case "000":
			$errormsg="Invalid error number. $s.\n";
			break;

		default:
			$errormsg="Unknown error $errorno. $s ";
	}
	print $errormsg. " ($lineno)";
	if($exit){
		print "Exiting.\n";
		exit(-1);
	}
}
?>