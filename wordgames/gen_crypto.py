import sys
import random

def encrypt(s):
	letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	random.shuffle(letters)
	
	for i in range(0,len(letters)):
		if chr(i+ord('A')) == letters[i]:
			letters [i],letters[i-1] = letters[i-1],letters[i]
	s = s.upper()
	cryptogram = ''
	for c in s:
		c_ord = ord(c)
		if c_ord >= ord('A') and c_ord <=ord('Z'):
			cryptogram += letters[c_ord-ord('A')]
		else:
			cryptogram += c
	return cryptogram
		
def pick_phrase(fname="cleartext.txt"):
	MIN_WORD_COUNT = 20
	MAX_WORD_COUNT = 60
	good_strings = []
	with open(fname,"rt") as f:
		for line in f:
			if len(line)< 10:# skip very short lines
				continue
			line = line.strip()
			wordcount = len(line.split(" "))
			if wordcount >= MIN_WORD_COUNT and wordcount <= MAX_WORD_COUNT:
				good_strings.append(line)
				#print(line)
	line_no = random.randrange(0,len(good_strings))
	return good_strings[line_no]

if __name__ == "__main__":
	clear = pick_phrase()
	crypto = encrypt(clear)
	print(clear)
	print(crypto)

