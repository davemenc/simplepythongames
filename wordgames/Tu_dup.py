"""Code To Check Merchant IDs Already Existing In TU workflow"""

import csv
import collections

m_name=list()
mcid=list()
csv_dict=dict()

with open("./TU_dem.txt") as tu_file:
    tu_file.seek(0,0)
    line=tu_file.readlines()
    for i in range(0,len(line)):
        if line[i].startswith("merchant {"):
            m_name.append(list(line[i][0:-1].split("# "))[1])
        elif line[i].startswith("  merchant_id"):
            mcid.append(int(list(line[i][0:-1].split())[1]))
        else:
            continue
tu_file.close()

merchants=dict(zip(mcid,m_name))

with open("./TUissues.csv") as csv_file:    
    csv_reader=csv.reader(csv_file)
    csv_data=[row for row in csv_reader]
    
    for i in range(1,len(csv_data)):
        if "CL for TU" in csv_data[i][1]:
            try:
                """bug_id and mcid"""
                last_rec=str(csv_data[i][0]+","+csv_data[i][1]+","+csv_data[i][2])
                csv_dict.update({int(csv_data[i][2]):int(csv_data[i][0])})
            except IndexError:
                print("Dictionary Not Created")
csv_file.close()
print("Last record processed:",last_rec)
print("MCIDs already present in the file:",set(mcid) & (set(csv_dict.values())))
print("MCIDs of duplicate bugs:",[item for item, count in collections.Counter(csv_dict.values()).items() if count > 1])
  
	
