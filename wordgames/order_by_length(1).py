import sys
def sortSecond(val):
    return val[1]
def sort_by_length(infile,outfile):
    lines = []
    with open(infile,"rt") as f:
      for line in f:
        line = line.strip()
        linelen = len(line)
        if linelen<40:
            continue
        lines.append((line,linelen))
    lines.sort(key=sortSecond,reverse=True)
    with open(outfile,"wt") as f:
        for line in lines:
            s = line[0]+"\n"
            f.write(s)
fname = "temp.txt"
fname2 = "sorted.txt"
sort_by_length(fname,fname2)
