import enchant
import itertools

DICTIONARY = enchant.Dict("en_US")


def is_en_word(w):
	return DICTIONARY.check(w)

def anagrams(word):
	results = []
	for l in itertools.permutations(list(w),len(w)):
		new_str = ''.join(l)
		if is_en_word(new_str):
			anagrams.append(new_str)
words = ["smug","ball","lard","zoos","pack","disc","mill","salt","cola","film","veto"]
print("____________________________________________________________________")
w = "smug"
anagrams = []
for l in itertools.permutations(list(w),len(w)):
	new_str = ''.join(l)
	if is_en_word(new_str):
		anagrams.append(new_str)
if len(anagrams)>1:
	print(w,anagrams)
else:
	print(w,"No Anagrams")