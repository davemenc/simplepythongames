<?php
/*
Copyright 2011 Dave Menconi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0
 */

include_once "debug_win.php";
include_once "errors.php";
include_once "specialcharlist.php";

$FirstTimeFlag = true; // this flag will create a complete set of output files; after that it will redo the first 10
if ($FirstTimeFlag) print"MODE: First Time\n";
else print"MODE: Not First Time\n";
date_default_timezone_set('America/Los_Angeles');

$filelistname = 'filelist.txt';
//print "filelistname=$filelistname\n";
$pathname = 'D:\\DEV\\words\\textfiles\\';
//print "pathname = $pathname\n";
$filenames = read_file($filelistname);//get the raw text from the file

// create the special character array

for($i= 0; $i<strlen($specstr); $i++) $specials[]=$specstr[$i];


$dictionary = read_dictionary("dictionary.txt");
$filecount=0;
$linenumber=0;
$hundreds =0;
foreach($filenames as $idx=>$fname){
	$wordcountlist = array();
	$filename = trim($fname);
	$filename = $pathname.$filename;
	$countfilename = $filename.".cnt";
	if($FirstTimeFlag && file_exists($countfilename)) {print "Skipping $filename -- already processed \n"; continue;}//first time through skip any outputs that are already there
	print "--- $filename --- \n";
	if(!file_exists($filename)) {
		print "!!!could not find file: $fname!\n";
		continue;
	}
	$fin = fopen($filename,"rt");
	if ($fin===false){
		print "*** error: could not open $filename for reading. \n";
		continue;
	}
//	else print "successfully opened $filename\n";

	while (($line = fgets($fin,8000))!== false ){
		parse_line($line);
		if($linenumber++>100){$linenumber=0;$hundreds++;print "$filename: $hundreds; ". date('l jS \of F Y h:i:s A')."\n";}
	}
	fclose($fin);
	print_words($wordcountlist,$countfilename);//create a count file
	print "$filecount: $countfilename\n";
	if(!$FirstTimeFlag && $filecount++>=9) break;//after the first time, only do 10 files
}


exit();
//--------- FUNCTIONS ----------
//------------------------------------
function parse_line($line){
	global $wordcountlist,$dictionary;
	//print "function parse_line($line)\n";
	$line = strtolower($line);
	$line = preg_replace('/[^ a-z]+/',' ', $line);
	$words = explode(" ",$line);
	foreach($words as $word){
		$word = trim($word);
		if(strlen($word)==0) continue;
//		if (!array_key_exists($word,$dictionary)) print "\t\t\tBad: $word\n"; else print "\tgood: $word\n";
		if (!array_key_exists($word,$dictionary)) continue;
		if(array_key_exists ($word,$wordcountlist))$wordcountlist[$word]++;
		else $wordcountlist[$word]=1;
	}
}
function count_words($words){
	$newlist=array();
	foreach ($words as $word){
		if(array_key_exists($word,$newlist)) $newlist[$word]++;
		else $newlist[$word]=1;
	}
	return $newlist;
}
//------------------------------------
function print_words($words,$outputfile){
	$fout = fopen($outputfile,"wt");
	foreach ($words as $word=>$count){
		fwrite($fout, "$word\t$count\n");
	}
}
//------------------------------------
function clean_word($word){
	global $specials;
//	print "function clean_word($word)\n";
	$word = trim($word);
	$word = strtolower ($word);
	$word = str_replace ($specials , "" , $word);

	return $word;
}
//------------------------------------
function replace_spec($line){
	global $specials;
	//print "function replace_spec($line)\n";
	$line = str_replace ($specials , " " , $line);

	return $line;
}
//------------------------------------
function read_dictionary($fname){
	$list = read_file("dictionary.txt");
	$dictionary= array_flip ( $list);

	return $dictionary;
}

//------------------------------------
function parse_file($filename){
	print "function parse_file($filename)\n";
	$s = getfile($filename);//get the raw text from the file
	$linearray = explode  ( "\n" , $s);//split it into lines

	print "lines=".count($linearray)."\n";
	$wordlist = array();
	foreach($linearray as $idx=>$line){
		$len = strlen($line);
//		print "$idx: $len: ";
		$line =  preg_replace('/[^(\x20-\x7F)]*/','', $line);
		$line = replace_spec($line);
		$words = explode(" ",$line);
//		$wordlist = array_merge($wordlist,$words);
		$wordlist[]=$words;
//		print count($words)."\n";
	}
	return ($wordlist);
}


//------------------------------------
/** putfile()
 * A routine the dumps text into a file
 *
 * @param $fname -- the fname to output to
 * @param $data -- the data to output
 * @return none
 * @sideeffects Another file on the disk
 * @author Dave Menconi
 */
function putfile($fname,$data){
	//print "function putfile($fname,data)\n";
	$fh = fopen($fname,'wt');
    if (false===$fh) emiterror(101,$fname,__LINE__ );
    $data = fwrite($fh,$data);
    if (false === $data) emiterror(103,$fname,__LINE__ );
    fclose($fh);
}
/** read_file()
 *
 *
 *
 *
 * @param $filename -- the file name to read data in from
 * @return $drink_distances -- the array data is stored in
 * @sideeffects populates $spec_drinks array
 *
 * @author Dave Menconi
 */

function read_file($filename){
//	print "function read_file($filename)\n";

	$s=getfile($filename);//get the raw text from the file
	$linearray = explode  ( "\n" , $s);//split it into lines
	return ($linearray);
}

//------------------------------------
/** getfile()
 * Desc
 *
 * @param
 * @return
 * @author Dave Menconi
 */
function getfile($fname){
	//print "function getfile($fname)\n";
	if(!file_exists($fname)) {
		print "!!!could not find file: $fname!\n";
		return "";
	}
	$fh = fopen($fname,'rt');
    if (false===$fh) emiterror(101,$fname,__LINE__ );
    $data = fread($fh,filesize($fname));
    if (false === $data) emiterror(102,$fname,__LINE__ );
    fclose($fh);
    return $data;
}

//------------------------------------
/** printusage()
 * Desc
 *
 * @param
 * @return
 * @author Dave Menconi
 */
function printusage(){
	//print "function printusage()\n";
	print "Usage is \n";
	print "parseluciferlist <datafile>\n";
	print "Where <datafile> is the filename containing a tab-delimited text file\n";
	exit();
}

?>
