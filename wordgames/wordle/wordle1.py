def duplicates(word):
	letters = set()
	for l in word:
		letters.add(l)
	if len(letters)==len(word):
		return False
	else:
		return True
def is_ascii(word):
	if isinstance(word,str):
		try:
			word.encode('ascii')
		except UnicodeEncodeError:
			return False
	else:
		try:
			word.decode('ascii')
		except UnicodeDecodeError:
			return False
	return True
def validword(word):
	if len(word)==5 and word.isalpha() and is_ascii(word) and not duplicates(word):
		return True

	return False

infile = "possible5letter.txt"
wordfile = "words_5letter.txt"
mylist = set()
with open(infile,"rt") as fin:
	for lines in fin:
		word = lines.strip().upper()
		if validword(word):
			mylist.add(word)
with open(wordfile,"wt") as fout:
	for word in mylist:
		fout.write(f"{word}\n")
		#print(word)
print(mylist,len(mylist))
