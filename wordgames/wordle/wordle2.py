wordfile = "words_5letter.txt"

mylist = set()
with open(wordfile,"rt") as fin:
	for lines in fin:
		word = lines.strip().upper()
		if len(word)==5:
			mylist.add(word)
total = len(mylist)*5
letters={"A":0,"B":0,"C":0,"D":0,"E":0,"F":0,"G":0,"H":0,"I":0,"J":0,"K":0,"L":0,"M":0,"N":0,"O":0,"P":0,"Q":0,"R":0,"S":0,"T":0,"U":0,"V":0,"W":0,"X":0,"Y":0,"Z":0}

scores = {"A":0,"B":0,"C":0,"D":0,"E":0,"F":0,"G":0,"H":0,"I":0,"J":0,"K":0,"L":0,"M":0,"N":0,"O":0,"P":0,"Q":0,"R":0,"S":0,"T":0,"U":0,"V":0,"W":0,"X":0,"Y":0,"Z":0}
for word in mylist:
	for i in range(0,len(word)):
		#print(word,len(word),i)
		letters[word[i:i+1]] += 1
		total += 1
#print (letters)
t2 = 0
for letter,cnt in letters.items():
	scores[letter] = cnt/total
outfile = "letterfreq5.txt"
with open(outfile,"wt") as fout:
	for letter,score in scores.items():
		fout.write(f"{letter}\t{score}\n")
		print(f"{score}\t{letter}")
