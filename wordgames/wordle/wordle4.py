def duplicates(word):
	letters = set()
	for l in word:
		letters.add(l)
	if len(letters)==len(word):
		return False
	else:
		return True
def read_scores(scorefile):
	scores = {}
	with open (scorefile,"rt") as fin:
		for line in fin:
			vals = line.strip().split("\t")
			letter = vals[0]
			score = float(vals[1])
			scores[letter]=score
	return scores
def read_words(wordfile):
	wordlist = set()
	with open(wordfile,"rt") as fin:
		for lines in fin:
			word = lines.strip().upper()
			if not duplicates(word):
				wordlist.add(word)
	return list(wordlist)
def words_with_letters(wordlist,letters_needed):
	result = set()
	maybe=True
	for word in wordlist:
		maybe=True
		for l in letters_needed:
			if l not in word:
				maybe=False
				break
		if maybe:
			result.add(word)

	return list(result)
def words_without_letters(wordlist,letters_not):
	result = set()
	for word in wordlist:
		maybe = True
		for l in letters_not:
			if l in word:
				maybe=False
		if maybe:
			result.add(word)
	return list(result)
if __name__ == "__main__":
	scorefile = "letterfreq5.txt"
	wordfile = "words_5letter.txt"

	letters_not = "RIOSCNTLU"
	letters_need = "AEBD"
	word_form = ""
	scores = read_scores(scorefile)
	wordlist = read_words(wordfile)
	print("ALL",len(wordlist))
	
	#find the words that have need 
	yes_list = words_with_letters(wordlist,letters_need)
	print("YES",len(yes_list))
	no_list = words_without_letters(yes_list,letters_not)
	print("NO",len(no_list))


	#score the words
	wordscore= {}
	for word in no_list:
		letterpool = set()
		for i in range(0,len(word)):
			letterpool.add(word[i:i+1])
		letterscore = 0
		for letter in letterpool:
			letterscore += scores[letter]
		wordscore[word]=letterscore
	scorelist = sorted(wordscore.items(), key=lambda x:x[1],reverse=True)

	limit = 20
	for wordscore in scorelist:
		w=wordscore[0]
		score=wordscore[1]
		if w[-1]=="E" and w[1:2]=="A":
			print(w,score)
			limit += -1
		if limit<=0:
			break

