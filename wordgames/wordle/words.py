def is_ascii(word):
	if isinstance(word,str):
		try:
			word.encode('ascii')
		except UnicodeEncodeError:
			return False
	else:
		try:
			word.decode('ascii')
		except UnicodeDecodeError:
			return False
	return True
def validword(word):
	if len(word)==5 and word.isalpha() and is_ascii(word):
		return True

	return False

wordfile = "/usr/share/dict/words"
outwordfile = "wordlist_len5.txt"
wordlist = set()
with open(wordfile,"rt") as fin:
	for line in fin:
		word = line.strip().upper()
		if validword(word):
			wordlist.add(word)
with open(outwordfile,"wt") as fout:
	for word in wordlist:
		fout.write(f"{word}\n")
			
