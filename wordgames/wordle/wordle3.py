def duplicates(word):
	letters = set()
	for l in word:
		letters.add(l)
	if len(letters)==len(word):
		return False
	else:
		return True
scorefile = "letterfreq5.txt"
scores = {}
with open (scorefile,"rt") as fin:
	for line in fin:
		vals = line.strip().split("\t")
		letter = vals[0]
		score = float(vals[1])
		scores[letter]=score
print(scores)
wordfile = "words_5letter.txt"
wordlist = set()

with open(wordfile,"rt") as fin:
	for lines in fin:
		word = lines.strip().upper()
		if len(word)==5:
			wordlist.add(word)
wordscore= {}
for word in wordlist:
	letterpool = set()
	for i in range(0,len(word)):
		letterpool.add(word[i:i+1])
	letterscore = 0
	for letter in letterpool:
		letterscore += scores[letter]
	wordscore[word]=letterscore
scorelist = sorted(wordscore.items(), key=lambda x:x[1],reverse=True)

print(scorelist)
for score in scorelist[0:20]:
	print(score)
#sortdict = dict(marklist)
#print(sortdict)
