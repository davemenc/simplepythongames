#makes anagrams
import enchant
import itertools

DICTIONARY = enchant.Dict("en_US")


def is_en_word(w):
	return DICTIONARY.check(w)

def anagrams(word):
	results = set()
	for l in itertools.permutations(list(w),len(w)):
		new_str = ''.join(l)
		if is_en_word(new_str):
			results.add(new_str)
	return list(results)


print("____________________________________________________________________")
words = ["aill","ball","band","body","bore","chum","cola","cork","disc","fang","film","firm","four","gear","grab","grin","hare","lacy","lard","mark","mill","pack","pill","race","salt","sewn","smug","stop","tend","till","veto","wait","wish","zoos"]

for w in words:
	print(w,len(anagrams(w))>1)


