import random
year = 0
land = 1000
population = 95
grain = 2800


rats = 200
starved = 0
babies = 5
do_plague = False
harvest_yield = 3
starved = 0
acres_per_person = 10
percent_starved = 0
total_starved = 0

def get_int_from_human(prompt,error,max_val):
	done = False
	while not done:
		from_human = raw_input("{0}: ".format(prompt))
		try:
			val = int(from_human)
			if val>max_val:
				print "{0}.".format(error)   
			elif val < 0:
				quit() #a bit harsh -- I guess the author felt the user was trying to cheat
			else:
				done = True
		except ValueError:
			print "Hammarabi, you must enter a valid number!"
	return val

def roll_die(sides):
	return int(random.random()*sides+1)
	
def introduction():
	print "Hamurabi"
	print "Inspired by David Ahl and Creative Computing, Morristown, New Jersey"
	print "Converted from BASIC to Python, 2017"
	print "\n\n"
	print "Try your hand at governing ancient Sumeria for a ten-year term of office\n"

def show_status():
	global year, starved,babies,population, land, harvest_yield, rats,grain
	print "\n\n\nHamurabi:  I beg to report to you,"
	print "in year {0}, {1} people starved, {2} came to the city,".format(year,starved,babies)
	print "the population is now {0}.".format(population)
	print "The city now owns {0} acres.".format(land)
	print "You harvested {0} bushels per acre.".format(harvest_yield)
	print "Rats ate {0} bushels.".format (rats)
	print "You now have {0} bushels of grain in storage.\n".format(grain)
	
def quit():
	print "Hamurabi: I cannot do what you wish."
	print "Get yourself another steward!!!!!"
	goodbye()	
	
def goodbye():
	print "So long for now!"
	exit()
	
def plague():
	global population
	population /= 2
	print "A horrible plague struck! Half the people died."

def you_lose():
	print "Due to this extreme mismanagement you have not only\nbeen impeached and thrown out of office but you have\nalso been declared national fink!!!!"
	goodbye()
	
def not_enough_grain():
	global grain
	print "Hamurabi think again. You have only {} bushels of grain. Now then, ".format (grain)

def not_enough_land():
	global land
	print "Hamurabi think again. You have only {} acres of land. Now then, ".format(land)

def ending():
	global percent_starved,  population, acres_per_person
	print "In your 10 yer term of office {0} percentage of the".format(percent_starved)
	print "population starved per year on average."
	print "{0} people zx!".format(total_starved)
	acres_per_person = land / population
	print "You started with 10 acres per person and ended with {0} acres per person.\n".format(acres_per_person)

	if percent_starved > 33 or acres_per_person < 7:
		you_lose()

	if percent_starved >10 or acres_per_person < 9:
		print "Your heavy-handed performance smacks of Nero and Ivan IV. \nThe people (remaining) find you an unpleasant ruler, and, \nfrankly, hate your guts!!"
		goodbye()
	if percent_starved > 3 or acres_per_person <10:
		print "Your performance could have been somewhat better, but \nreally wasn't too bad at all. {} people \nwould dearly like to see you assassinated but we all have our \ntrivial problems.".format(int(random.random()*population*.8))#'
		goodbye()
	print "A fantastic performance!!!  Charlemange, Disraeli, \nand Jefferson combined could not have done better!"
	goodbye()

def main():
	global year,land,population,grain,rats,starved,babies,do_plague,harvest_yield,starved,acres_per_person,percent_starved,total_starved
	
	introduction()
	for year in range (1,11):
		
		#START OF YEAR... new people and a plague!
		population += babies
		if do_plague: plague()
		show_status()#270
		
		#BUY LAND
		zero_nine = roll_die(10)-1
		land_price = zero_nine+17 #310
		print "Land is trading at {} bushels per acre.".format(land_price) #312
		buy_acres = get_int_from_human("How many acres do you wish to buy","Hamurabi:  think again.  You have only {} bushels of grain.  Now then,".format(grain),
			int(grain/land_price)) #320-330
		land += buy_acres
		grain -= buy_acres*land_price
		
		#sell land
		if buy_acres == 0:#330
			sell_acres = get_int_from_human("How many acres do you wish to sell" , 
				"Hamurabi:  think again.  You own only {} acres.  Now then,".format(land),
				land)
			land -= sell_acres
			grain += sell_acres*land_price
		
		#FEED PEOPLE
		print
		grain_for_food = get_int_from_human("How many bushels do you wish to feed your people",
			"Hamurabi:  think again.  You have only {} bushels of grain.  Now then,".format(grain),
			grain) #410-422
		grain -= grain_for_food #430
		
		#Rats Are Running Wild (moved up because rats only eat pre-harvest grain)
		rats = 0
		five_sided = roll_die(5)#521
		if five_sided%2 == 0: #522
			rats = int(grain/five_sided) # rats eat pre-harvest grain, but you can plant with rat-eaten grain
		
		#PLANT
		print 
		valid_plant = False
		while not valid_plant:
			plant_land = get_int_from_human("How many acres do you wish to plant with seed",
				"Hamurabi:  think again.  You own only {} acres. Now then,".format(land),
				land) #440-447
			#enough grain?
			if int(plant_land/2) > grain: #450
				print "Hamurabi:  think again.  You have only {} bushels of grain.  Now then,".format(grain)
				continue 
			#enough people?
			if plant_land > 10 * population: #455
				print "But you have only {} people to tend the fields!  Now then, ".format(population)
				continue
			valid_plant = True
		grain -= int(plant_land/2) #510

		#Bountiful Harvest #512
		five_sided = roll_die(5)#511
		harvest_yield = five_sided  #515
		harvest = harvest_yield * plant_land
		grain += harvest
		
		#NOW apply rats
		grain -= rats
		
		#babies
		five_sided = roll_die(5) #531
		babies = int(five_sided *(20*land+grain) / population / 100+1)#533
		
		#full tummies
		fed_people = (grain_for_food/20)#540
		
		#check for plague
		#plague_roll = int(10*(2*random.random()-.3)) #542 that works out to 19 values and we look for 16 to avoid so that's 15.7
		do_plague = roll_die(1000)<=157 #542 
		
		#check starvation
		starved = population - fed_people #552
		if starved > .45* population:
			print "\nYou starved {} people in one year!!!".format(starved)
			you_lose()
		
		#Percent starved 
		percent_starved = ((year -1)*percent_starved + starved*100/population)/year #553
		
		#people die of starvation
		population = fed_people #555
		total_starved += starved
		print
		
	show_status()
	ending()
	goodbye()
	
if __name__ == "__main__": 
	main()