When I was learning to program the Big Language was BASIC. The way we learned BASIC was to write little games and modify other people's games. There were whole books filled with BASIC games.

I am (mildly) concerned that people who want to learn Python just have to come up with something serious to code. 

So I thought I would write a few games. These are not PyGames type games. These are ultra simple text based games in the spirit of the old BASIC games -- indeed, simpler than most of them. 

The major part of this is so they'll be very easy to understand. But also, if you create a really simple game people will want to change it. Go for it!