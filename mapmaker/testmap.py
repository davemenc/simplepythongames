from arraymanager import ArrayManager
import rules
def sum_adj(adj):
	sum = 0
	for v in adj:
		sum+=v
	return sum

LAND=1
WATER=1000
WIDTH=20
HEIGHT=10
am = ArrayManager(WIDTH, HEIGHT)
start = (WIDTH//2,HEIGHT//2)
am.set_tuple(start,LAND)
am.display()

MAX=10
count = MAX
queue = []
queue.append(start)
while len(queue) and count:
	count-=1
	cur = queue.pop()
	adj = am.get_adjacent_tup(cur)
	sum = 0
	for sqr in adj:
		sum += adj[sqr]
		if not am.get_tup(sqr):
			queue.append(sqr)
	print(adj)
	print(sum)
	print(queue)
	exit()

