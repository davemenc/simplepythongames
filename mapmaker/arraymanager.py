class ArrayManager:
	def __init__(self, cols, rows):
		self.cols = cols
		self.rows = rows
		# Initialize a 2D array with zeros
		self.array = [[0] * cols for _ in range(rows)]
	
	def display(self):
		for y in range(0,self.rows):
			for x in range(0,self.cols):
				print(self.get_value(x,y),end="")
			print()
	
	def dimensions(self):
		return(self.cols,self.rows)
	
	def save_array(self,fname):
		with open(fname,"wt") as f:
			f.write(f"{self.rows}	{self.cols}\n")
			for y in range(0,self.rows):
				for x in range(0,self.cols):
					f.write(f"{self.get_value(x,y)}\t")
				f.write("\n")
	
	def set_value(self, x, y, value):
		if 0 <= x < self.cols and 0 <= y < self.rows:
			self.array[y][x] = value
			return True
		else:
			print(f"Invalid coordinates in set_value. {x},{y}")
			return False
	def set_tuple(self,tup,value):
		x = tup[0]
		y = tup[1]
		self.set_value(x,y,value)

	def get_value(self, x, y):
		if 0 <= x < self.cols and 0 <= y < self.rows:
			return self.array[y][x]
		else:
			print(f"Invalid coordinates in get_value. {x},{y}")
			return None

	def get_tuple(self,tup):
		x = tup[0]
		y = tup[1]
		return (self.get_value(x,y))

	def get_adjacent_tup(self,tup):
		x=tup[0]
		y=tup[1]
		return self.get_adjacent(x,y)
			
	def get_adjacent(self, x, y):
		adjacent_numbers = {}
		for i in range(max(0, y - 1), min(self.rows, y + 2)):
			for j in range(max(0, x - 1), min(self.cols, x + 2)):
				if i != y or j != x:
					adjacent_numbers[(i,j)] = self.array[i][j]
		return adjacent_numbers

if __name__=="__main__":
	print("SYNTAX GOOD")
	# Example Usage:
	am = ArrayManager(4, 3)
	print(am.dimensions())

	# Set values
	am.set_value(0, 0, 'a')
	am.set_value(1, 0, 'b')
	am.set_value(2, 0, 'c')
	am.set_value(1, 1, 'd')
	for x in range(0,am.cols):
		am.set_value(x,1,'e')
	# Get values
	print("Value at (0, 1):", am.get_value(0, 1))

	# Get adjacent numbers
	print("Adjacent numbers to (0, 1):", am.get_adjacent(0, 1))
	am.display()
	am.save_array("test.sav")
	print("TESTS COMPLETED")