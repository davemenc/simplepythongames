import random  
from turtle import *
from datetime import date

from freegames import square

cells = {}

def load_pattern(fname):
    pattern = set()
    with open(fname,"rt") as f:
        for line in f:
            data = line.strip().split(",")
            x=data[0]
            y=data[1]
            pattern.add((x,y))
    return pattern

def save_pattern(fname,pattern):    
    pattern = set(pattern)
    with open(fname,"wt") as f:
        for p in pattern:
            f.write(f"{p[0]},{p[1]}\n")

def initialize(size=[],array=None):
    """Randomly initialize the cells."""
    if len(size)!=4:
        setup(420, 420, 370, 0)
    else:
        setup(size[0],size[1],size[2],size[3],)
    hideturtle()
    tracer(False)
    for x in range(-200, 200, 10):
        for y in range(-200, 200, 10):
            cells[x, y] = False
    if array is None:
        for x in range(-50, 50, 10):
            for y in range(-50, 50, 10):
                cells[x, y] = random.choice([True, False])
    else:
        for data in size:
            x = data[0]
            y = data[1]
            cells[x,y] = True



def step():
    """Compute one step in the Game of Life."""
    neighbors = {}

    for x in range(-190, 190, 10):
        for y in range(-190, 190, 10):
            count = -cells[x, y]
            for h in [-10, 0, 10]:
                for v in [-10, 0, 10]:
                    count += cells[x + h, y + v]
            neighbors[x, y] = count
    changes = 0
    for cell, count in neighbors.items():
        if cells[cell]:
            if count < 2 or count > 3:
                cells[cell] = False
                changes+=1
        elif count == 3:
            cells[cell] = True


def draw():
    """Draw all the squares."""
    clear()
    for (x, y), alive in cells.items():
        #color = 'green' if alive else 'black'
        if alive:
            square(x, y, 10, 'green')
        else: 
            square(x, y, 10, 'black')

    update()
    #ontimer(draw, 100)

if __name__ == "__main__":
    PATTERN_FNAME = "blinker.dat"
    today = date.today()
    print(str(today))
    new_pattern_fname = PATTERN_FNAME+str(today)

    pattern=load_pattern(PATTERN_FNAME)
    save_pattern(new_pattern_fname,pattern)

    print(pattern)
    initialize([],pattern)
    for i in range (0,10):
        draw()
        step()
    done()
