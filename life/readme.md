# Conway's Game of Life
This program displays the Game of Life. It generates a random pattern and then just cranks away. 

## Strategy
This is a very brute force strategy: it checks every square then goes back and decides which ones should live or die.

## Rules
The rules of Conway's Game of Life are pretty simple. 
You start with a grid. Some grid squares are "occupied" and some are empty. 
For every "generation" you count the number of occupied squares next to ever grid square. 
An occupied square with 2 or 3 neighbors lives. 
An unoccupied square with exactly 3 neighbors becomes occupied.
Otherwise the square becomes empty. 

You do this over and over again and it looks like it's being animated according to some complex algorithm but it's really very, very simple. 
