import os, pygame, sys,json,time,copy
from pygame.locals import *
SCREENX=1920
SCREENY=1080
WHITE =     (255, 255, 255)
BLUE =      (  0,   0, 255)
GREEN =     (  0, 255,   0)
RED =       (255,   0,   0)
TEXTCOLOR = (  0,   0,  0)
(width, height) = (200, 300)
RADIUS = 50
SPACING = 10
STARTX=80
STARTY = 80
COLW = 6
COLH = 6
def main():
	pygame.init()
	DISPLAYSURF = pygame.display.set_mode(( SCREENX, SCREENY),pygame.FULLSCREEN)
	DISPLAYSURF.fill((255,255,255))
	pygame.display.set_caption('')
	pygame.mouse.set_visible(1)
	clock = pygame.time.Clock()
	pygame.display.update()

	colx = 0
	coly = 0
	# main loop
	going = True
	changed = True
	player_state=-1
	posx=STARTX
	posy=STARTY
	curcol=RED
	#pygame.draw.circle(DISPLAYSURF, GREEN, [STARTX,STARTY], 20)
	print("green",STARTX,STARTY)

	while going:
		for event in pygame.event.get():
			if event.type == QUIT:
				going = False
			elif event.type == KEYDOWN and event.key == K_ESCAPE:
				going = False
			elif event.type == KEYDOWN or event.type == MOUSEBUTTONDOWN:
				#print('keydown')
				player_state+=1
				changed = True
				break
		curcol = RED

		if going and changed:
			changed = False

			if player_state>=0 and player_state<=35:
				curcol = RED
				colx = player_state % COLW
				coly = player_state//COLW
				posx=STARTX+(RADIUS*2+SPACING)*colx
				posy=STARTY+(RADIUS*2+SPACING)*coly

				pygame.draw.circle(DISPLAYSURF, curcol, [posx,posy], RADIUS)
			elif player_state>35 and player_state<72:
				curcol = BLUE

				colx = (player_state-36) % COLW
				coly = (player_state-36)//COLW
				posx=STARTX+(RADIUS*2+SPACING)*colx
				posy=STARTY+(RADIUS*2+SPACING)*coly

				pygame.draw.circle(DISPLAYSURF, curcol, [posx,posy], RADIUS)
			else:
				player_state=-1

			#print(player_state,colx,coly,posx,posy,curcol)

			pygame.display.update()

	pygame.quit()
main()
