import pygame
class Balloon: 
	def __init__(self,surface,posx,posy,diam,color,background = (0,0,0),maxy=1080):
		self.surface = surface
		self.posx=posx
		self.posy=posy
		self.diam = diam
		self.color = color
		self.popped = False
		self.background = background
		self.maxy=maxy
	def __repr__(self):
		return f"{self.posx}; {self.posy}; {self.diam}; {self.color}; {self.popped}"
	def animate(self):
		#print("animate",self)
		FALL = 1 # distance to fall every animation cycle
		# erase
		pygame.draw.circle(self.surface, self.background, [self.posx,self.posy], self.diam)


		# fall
		self.posy += FALL
		if self.posy> self.maxy: 
			self.popped
		# popped
		if self.popped:
			self.diam/=2

		# erased?
		if self.diam<1:
			return False

		# redraw
		pygame.draw.circle(self.surface, self.color, [self.posx,self.posy], self.diam)
		return True

	def pop (self):
		self.popped = True
	def collision(self,clickx,clicky):
		minx=self.posx-self.diam
		miny=self.posy-self.diam
		maxx=self.posx+self.diam
		maxy=self.posy+self.diam
		if clickx >= minx and clickx <= maxx and clicky >= miny and clicky <= maxy:
			return True
		else:
			return False
if __name__ == "__main__":
	print ("Balloon parses!")
	b = Balloon("screen",100,200,50,(255,0,0))
	print(b.surface,b.posx,b.posy,b.diam,b.color,b.popped)
	b.pop()
	print (b.popped)
	print(b.collision(110,220),b.collision(250,300))