import os, pygame, sys,json,time,copy
from pygame.locals import *
import pygame

""" 
THis program shows a bunch of circles. Previously called sammy2, I thin we should call it "cirlces"
when you tap any key it adds a circle
"""

charlist = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','9','0',',','.','?']

SCREENX=1920
SCREENY=1080
WHITE =     (255, 255, 255)
BLUE =      (  0,   0, 255)
GREEN =     (  0, 255,   0)
YELLOW =    (255, 255,   0)
RED =       (255,   0,   0)
BLACK =     (  0,   0,   0)
TEXTCOLOR = BLACK

(width, height) = (200, 300)
RADIUS = 50
SPACING = 10
STARTX=80
STARTY = 80
COLW = 6
COLH = 6
def main():
	pygame.init()
	DISPLAYSURF = pygame.display.set_mode(( SCREENX, SCREENY),pygame.FULLSCREEN)
	DISPLAYSURF.fill((255,255,255))
	pygame.display.set_caption('')
	pygame.mouse.set_visible(1)
	clock = pygame.time.Clock()
	pygame.display.update()

	colx = 0
	coly = 0
	# main loop
	going = True
	changed = False
	player_state=-1
	posx=STARTX
	posy=STARTY
	curcol=RED
	curchar = " "
	font = pygame.font.Font("ARLRDBD.TTF", 32)
	colx=0
	coly=0

	while going:
		for event in pygame.event.get():
			if event.type == QUIT:
				going = False
			elif event.type == KEYDOWN and event.key == K_ESCAPE:
				going = False
			elif event.type == KEYDOWN:
				player_state+=1
				changed = True
				curchar = event.unicode
				if curchar not in charlist:
					curchar=" "
				break
			elif event.type == MOUSEBUTTONDOWN:
				player_state+=1
				changed = True
				break
		curcol = RED

		if going and changed:
			changed = False

			if player_state>=0 and player_state<36:
				curcol = RED
			elif player_state>=36 and player_state<72:
				curcol = YELLOW
			elif player_state>=72 and player_state<108:
				curcol=BLUE
			elif player_state>=108 and player_state<144:
				curcol=GREEN
			elif player_state>=144:
				player_state=0
			colx = player_state % COLW
			coly = (player_state//COLW)%6
			posx=STARTX+(RADIUS*2+SPACING)*colx
			posy=STARTY+(RADIUS*2+SPACING)*coly
			#print(player_state,colx,coly,posx,posy,curcol)
			pygame.draw.circle(DISPLAYSURF, curcol, [posx,posy], RADIUS)
			text = font.render(curchar.upper(), True, TEXTCOLOR, curcol)
			textRect = text.get_rect()
			textRect.center = (posx, posy)
			DISPLAYSURF.blit(text, textRect)


			pygame.display.update()

	pygame.quit()
main()
