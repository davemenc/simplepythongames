# Sammy Game

## Sounds
The sounds are from [SoundBible by Mike Koenig](https://soundbible.com/2009-A-Z-Vocalized.html). They are shared through the Attribution 3.0 license. The original was one file with all the letters; I broke it up into 26 files each with 1 letter.
Thanks, Mike!

## 2023-05-30
* version 2 needs a few fixes
	* https://menconi.atlassian.net/browse/TODO-538
* version 3 needs some work
	* https://menconi.atlassian.net/browse/TODO-539
	* https://menconi.atlassian.net/browse/TODO-540
	
## 2023-05-29
* Version 1 works
* version 2 will include
	* a batch file so when the OS drops us out we can get back in
	* place a letter in the current circle that represents that key that was pressed.
## try to connect to jira
Worked...
## 2023-05-27
* 6x6 grid of circles
	* more or less centered on screen
    * redone to left justify on laptop screen
* starts red
* turns blue
* cycles back

## Original Notes
I want to do a game for sammy. 
* we'll start a  laptop and run the game
* she'll be able to type any key and it will do something in the game
* Also clicking the mouse. 
* there will be no way out of the game (I'd like to come up with a clever way but I think a reboot will be required)
* I was thinking of a 6x6 grid of the circles that first drew itself then changed colors
	* that is each button press adds a circle or changes a color
* I'm also thinking of adding letters to the circles
* there might, eventually be multiple games like this
* this is going to be a real pygame type game
