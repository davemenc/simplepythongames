import os, pygame, sys,json,time,copy
from pygame.locals import *
import pygame
import balloon
import random

""" 
This game features falling balloons. 
It might be called "sammy3" but lets start calling things by their names...
"""

SCREENX=1920
SCREENY=1080
WHITE =     (255, 255, 255)
BLUE =      (  0,   0, 255)
GREEN =     (  0, 255,   0)
RED =       (255,   0,   0)
PURPLE = 	(255,   0, 255)
AQUA =    (  0, 255, 255)
ORANGE = (179,100,0)
YELLOW =    (255, 255,   0)
BLACK =     (  0,   0,   0)
TEXTCOLOR = WHITE
BACKGROUND = BLACK

(width, height) = (200, 300)
RADIUS = 50
SPACING = 10
STARTX=80
STARTY = 80
COLW = 6
COLH = 6
def spawn(surface,colx,diam,color,bgcolor):
	return (Balloon(surface,colx,0,color,bgcolor))
def main():
	pygame.init()
	DISPLAYSURF = pygame.display.set_mode(( SCREENX, SCREENY),pygame.FULLSCREEN)
	DISPLAYSURF.fill(BACKGROUND)
	pygame.display.set_caption('BALLOONS')
	pygame.mouse.set_visible(1)
	clock = pygame.time.Clock()
	pygame.display.update()

	colx = 0
	coly = 0
	# main loop
	going = True
	changed = False
	player_state=-1
	posx=STARTX
	posy=STARTY
	curcol=RED
	curchar = " "
	font = pygame.font.Font("ARLRDBD.TTF", 32)
	colx=0
	coly=0
	turn = 0
	start_time = 0


	# tracking balloons
	spawn_interval = 100
	time_step = 20 # higher numbers make it slower;20 is nice
	balloon_start = [0,1,2,3,4,5,6,7,8,9,10]
	random.shuffle(balloon_start)
	bs_idx = 0
	balloon_list = []
	b_colors = [RED,BLUE,GREEN,ORANGE,PURPLE,YELLOW]
	random.shuffle(b_colors)
	bc_idx =0
	

	while going :
		button = False
		for event in pygame.event.get():
			if event.type == QUIT:
				going = False
			elif event.type == KEYDOWN and event.key == K_ESCAPE:
				going = False
			elif event.type == MOUSEBUTTONDOWN:
				#print("mousebutton detected")
				button = True
		if button:
			clickx,clicky=pygame.mouse.get_pos()
			clickx
			clicky
			#print("button",clickx,clicky)
			for b in balloon_list:
				if b.collision(clickx,clicky):
					#print('pop called')
					b.pop()
					break
		now = pygame.time.get_ticks()
		if going and now-start_time>=time_step:
			start_time = now
			delete=None
			for i in range(0,len(balloon_list)):
				if not balloon_list[i].animate():
					delete = i
			if delete is not None:
				del(balloon_list[delete])


			if turn % spawn_interval==0:
				if bs_idx>=len(balloon_start):
					bs_idx=0
					random.shuffle(balloon_start)
				spawncol = balloon_start[bs_idx]
				spawnx=STARTX+(RADIUS*2+SPACING)*spawncol
				bs_idx+=1
				if bc_idx>=len(b_colors):
					bc_idx=0
					random.shuffle(b_colors)

				spawnc = b_colors[bc_idx]
				bc_idx+=1
				b = balloon.Balloon(DISPLAYSURF,spawnx,0,RADIUS,spawnc,BACKGROUND)
				balloon_list.append(b)
				#print(b)
			turn+=1
			pygame.display.update()

	pygame.quit()
main()
"""
while run:
    ...
    now = pygame.time.get_ticks()
    if now - start_time > 1000 #if if has been 1 second (1000 millaseconds)
       #move squares
       start_time = now #reset timer

	colx = player_state % COLW
	coly = (player_state//COLW)%6
	posx=STARTX+(RADIUS*2+SPACING)*colx
	posy=STARTY+(RADIUS*2+SPACING)*coly
	print(player_state,colx,coly,posx,posy,curcol)
	pygame.draw.circle(DISPLAYSURF, curcol, [posx,posy], RADIUS)
	text = font.render(curchar.upper(), True, TEXTCOLOR, curcol)
	textRect = text.get_rect()
	textRect.center = (posx, posy)
	DISPLAYSURF.blit(text, textRect)

"""